package fr.pixelscrafters.survie.commands;

import fr.pixelscrafters.survie.managers.FactionManager;
import fr.pixelscrafters.survie.objects.factions.SFaction;
import fr.pixelscrafters.survie.objects.players.SPlayer;
import fr.pixelscrafters.survie.utils.items.ItemFactory;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class AdminFactionCommand extends AbstractInventoryCommand {
    public static final String INVENTORY_NAME = "Admin Factions Main Menu";
    public static final int NEXT_PAGE_SLOT = 35;
    public static final int PREVIOUS_PAGE_SLOT = 34;
    public static final int ADD_FACTION_SLOT = 33;
    public static final int FACTION_MAX_SLOT = 32;

    public AdminFactionCommand() {
        super(INVENTORY_NAME);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player){
            // je suis un joueur
            Player p = (Player) commandSender;
            if(p.hasPermission("surv.admin.managefactions")){
                p.openInventory(getMainMenu(1));
            } else {
                p.sendMessage("Vous n'avez pas la permission");
            }

        } else {
            // je suis pas un joueur
            commandSender.sendMessage("impossible de faire la commmande car tu n'es pas un joueur");
        }
        return true;
    }

    public static Inventory getMainMenu(int n){

        Inventory inventory = Bukkit.createInventory(null,9*4, INVENTORY_NAME);
        if(FactionManager.INSTANCE.getSFactions().size() - (n * FACTION_MAX_SLOT) > 0) {
            inventory.setItem(NEXT_PAGE_SLOT, ItemFactory.INSTANCE.createItem(Material.GREEN_DYE, "Suivant",
                    "Vers page : " + (n + 1)));
        }
        if(n != 1){
            inventory.setItem(PREVIOUS_PAGE_SLOT,ItemFactory.INSTANCE.createItem(Material.YELLOW_DYE,"Précédent",
                    "Vers page : " + (n-1)));
        }
        inventory.setItem(ADD_FACTION_SLOT,ItemFactory.INSTANCE.createItem(Material.BLUE_DYE,"Ajouter une faction",
                ""));

        try {
            for (int i = (n - 1) * FACTION_MAX_SLOT; i <= (n - 1) * FACTION_MAX_SLOT + FACTION_MAX_SLOT; i++) {
                SFaction sFaction = FactionManager.INSTANCE.getSFactions().get(i);
                inventory.addItem(ItemFactory.INSTANCE.createItem(Material.APPLE, sFaction.getName(),
                        sFaction.getUuid().toString()));
            }
        } catch (IndexOutOfBoundsException ex){

        }
        return inventory;
    }

    public void onInventoryClick(InventoryClickEvent event){
        event.setCancelled(true);
        Player p = (Player) event.getWhoClicked();
        if(event.getSlot() == AdminFactionCommand.ADD_FACTION_SLOT){
            SPlayer sPlayer = new SPlayer(p);
            SFaction faction = new SFaction("Entrez un nom", sPlayer);
            FactionManager.INSTANCE.addSFaction(faction);
            p.openInventory(AdminFactionCommand.getMainMenu(1));

        }else if(event.getSlot() == AdminFactionCommand.NEXT_PAGE_SLOT ||
                event.getSlot() == AdminFactionCommand.PREVIOUS_PAGE_SLOT){
            ItemStack itemStack = event.getCurrentItem();
            if(itemStack != null){
                List<String> lore = itemStack.getItemMeta().getLore();
                String line = lore.get(0);
                int pageNumber = Integer.parseInt(line.substring(12));
                p.openInventory(AdminFactionCommand.getMainMenu(pageNumber));
            }
        }
    }

}
