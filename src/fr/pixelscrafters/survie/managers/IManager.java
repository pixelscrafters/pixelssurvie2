package fr.pixelscrafters.survie.managers;

import fr.pixelscrafters.survie.utils.persistance.IDataAdapter;

/**
 * Manager interface.
 */
public interface IManager {

    /**
     * Loads data when the server starts.
     */
    public void load(IDataAdapter dataReader);

    /**
     * Saves data when the server stops or with autosave.
     */
    public void save(IDataAdapter dataReader);
}
