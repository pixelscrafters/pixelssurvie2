package fr.pixelscrafters.survie.objects.factions;

import fr.pixelscrafters.survie.objects.players.SPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SFaction {
    private UUID uuid;
    private String name;
    private List<UUID> members;
    private List<UUID> operators;
    private List<UUID> wantsToJoin = new ArrayList<>();
    private int money;
    private int plusMoneyQuest;
    private int plusMoneyShop;
    private int plusMoneyOthers;
    private int lessMoneyShop;
    private int lessMoneyOthers;

    public SFaction(String name, SPlayer operator){
        uuid = UUID.randomUUID();
        members = new ArrayList<>();
        operators = new ArrayList<>();
        operators.add(operator.getUuid());
        this.name = name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public int getMoney() {
        return money;
    }

    public int getPlusMoneyQuest() {
        return plusMoneyQuest;
    }

    public int getPlusMoneyShop() {
        return plusMoneyShop;
    }

    public int getPlusMoneyOthers() {
        return plusMoneyOthers;
    }

    public int getLessMoneyShop() {
        return lessMoneyShop;
    }

    public int getLessMoneyOthers() {
        return lessMoneyOthers;
    }

    public String getName() {
        return name;
    }

    public List<UUID> getMembers() {
        return members;
    }

    public List<UUID> getOperators() {
        return operators;
    }

    public List<UUID> getWantsToJoin() {
        if(wantsToJoin == null){
            wantsToJoin = new ArrayList<>();
        }
        return wantsToJoin;
    }

    public void addWantsToJoin(SPlayer sPlayer){
        if(wantsToJoin == null){
            wantsToJoin = new ArrayList<>();
        }
        if(!wantsToJoin.contains(sPlayer.getUuid())) {
            wantsToJoin.add(sPlayer.getUuid());
        }
    }

    public void removeWantsToJoin(UUID uuid){
        wantsToJoin.remove(uuid);
    }

    public boolean isOperator(SPlayer sp){
        UUID uuid = sp.getUuid();
        return operators.contains(uuid);
    }

    public void rename(String name){
        this.name = name;
    }

    public void removeMember(SPlayer sp){
        this.members.remove(sp.getUuid());
    }

    public void removeOperator(SPlayer sp){
        this.operators.remove(sp.getUuid());
        this.members.add(sp.getUuid());
    }

    public void addOperator(SPlayer sp){
        this.members.remove(sp.getUuid());
        this.operators.add(sp.getUuid());
    }

    public long countOnlinePlayers(){
        List<UUID> membersAndOp = new ArrayList<>(members);
        membersAndOp.addAll(operators);
        return membersAndOp.stream()
                .filter(x -> {
                    if(Bukkit.getOfflinePlayer(x) != null) {
                        return Bukkit.getOfflinePlayer(x).isOnline();
                    }
                    return false;
                }).count();
    }
}
