package fr.pixelscrafters.survie.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import fr.pixelscrafters.survie.main.PluginMain;
import fr.pixelscrafters.survie.managers.exceptions.AlreadyExistsException;
import fr.pixelscrafters.survie.objects.ranking.RankingHologram;
import fr.pixelscrafters.survie.utils.persistance.IDataAdapter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;

public class RankingManager implements IManager{
    public static final RankingManager INSTANCE = new RankingManager();
    private static final String FILE_LOCATION = "plugins/PixelsSurvie/rankingHolograms.json";
    private ReentrantLock lock = new ReentrantLock();
    private ArrayList<RankingHologram> holograms;
    private boolean flagLoadedSuccessfully = false;

    protected RankingManager(){

    }

    @Override
    public void load(IDataAdapter dataAdapter) {
        //LOADING DATA
        lock.lock();
        Bukkit.getLogger().info("Loading ranking holograms data...");
        String jsonRaw = dataAdapter.getTextData(FILE_LOCATION);
        try {
            Gson gson = new GsonBuilder().create();
            holograms = gson.fromJson(jsonRaw, new TypeToken<List<RankingHologram>>() {
            }.getType());
            flagLoadedSuccessfully = true;
            Bukkit.getLogger().info("Ranking holograms data loaded.");
            if(holograms != null) {
                Bukkit.getLogger().info("Loaded " + holograms.size() + " value(s).");
            } else {
                Bukkit.getLogger().info("The list of ranking holograms seems to be empty ...");
                holograms = new ArrayList<>();
            }
        } catch (JsonParseException ex){
            Bukkit.getLogger().severe("Error when loading ranking holograms data !\n\n" + ex.getMessage());
            Bukkit.getLogger().severe("§4§lServer will be switched off now !");
            Bukkit.getServer().shutdown();
        } finally {
            //PLACE ALL HOLOGRAMS AND RUN UPDATE TICK EVERY SECOND
            if(holograms == null){
                holograms = new ArrayList<>();
            }
            for(RankingHologram holo : holograms)
            {
                holo.createHologram();
            }

            new BukkitRunnable() {
                @Override
                public void run() {
                    try {
                        runTick();
                    }catch (Exception ex){
                        Bukkit.getLogger().log(Level.WARNING, "An error as occured while updating ranks.\n"
                                + ex.getLocalizedMessage());
                    }
                }
            }.runTaskTimer(PluginMain.getInstance(), 20L, 20L);
            lock.unlock();
        }
    }

    @Override
    public void save(IDataAdapter dataAdapter) {
        //SAVING DATA
        lock.lock();
        try {
            Bukkit.getLogger().info("Saving ranking holograms data...");
            if (flagLoadedSuccessfully) {
                Gson gson = new GsonBuilder().create();
                dataAdapter.writeTextData(FILE_LOCATION, gson.toJson(holograms));
                Bukkit.getLogger().info("Saved successfully !");
            } else {
                Bukkit.getLogger().warning("Cannot save ranking holograms data : An error occurred while loading them," +
                        " please fix this before trying to start your server.");
            }
        }finally {
            lock.unlock();
        }
    }

    private void runTick(){
        for(RankingHologram holo : holograms){
            holo.update();
        }
    }

    public void addNewRankingHologram(Location l, String name, RankingHologram.RankingType type) throws AlreadyExistsException {
        if(holograms.stream().filter(x-> x.getName().equals(name)).count() > 0){
            throw new AlreadyExistsException(name);
        }
        RankingHologram rankingHologram = new RankingHologram(name, l.getBlockX(), l.getBlockY(),
                l.getBlockZ(), l.getWorld().getName(), type);
        rankingHologram.createHologram();
        holograms.add(rankingHologram);
    }

    public void removeRankingHologram(String name){
        try{
            RankingHologram holo = holograms.stream().filter(x-> x.getName().equals(name)).findFirst().get();
            holo.destroy();
            holograms.remove(holo);
        } catch(NoSuchElementException ex){

        }
    }
}
