package fr.pixelscrafters.survie.utils.persistance;

import org.bukkit.Bukkit;

import java.io.*;

public class DiskData implements IDataAdapter {
    @Override
    public String getTextData(String location) {
        File f = new File(location);
        try {
            InputStream is = new FileInputStream(f);
            return readStream(is);
        } catch (FileNotFoundException e) {
            Bukkit.getLogger().severe("Cannot find file " + location);
        }
        return null;
    }

    @Override
    public void writeTextData(String location, String text) {
        File f = new File(location);

        try (PrintStream out = new PrintStream(new FileOutputStream(f), true, "UTF8")) {
            out.print(text);
        }
        catch (FileNotFoundException e) {
            Bukkit.getLogger().severe("Cannot find file " + location);
        } catch (UnsupportedEncodingException e) {
            Bukkit.getLogger().severe("Error" + e.getMessage());
        }
    }

    private static String readStream(InputStream is) {
        StringBuilder sb = new StringBuilder();
        try {
            Reader r = new InputStreamReader(is, "UTF-8");
            int c = 0;
            while ((c = r.read()) != -1) {
                sb.append((char) c);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }
}
