package fr.pixelscrafters.survie.objects.blocs;

import fr.pixelscrafters.survie.objects.factions.SFaction;

import java.util.UUID;

public abstract class AbstractObject {
    private UUID identifier;
    private MapObjects objectType;
    private UUID sPlayerOwner;
    private int pcu;
    private SFaction faction;

    public UUID getIdentifier() {
        return identifier;
    }

    public MapObjects getObjectType() {
        return objectType;
    }

    public UUID getsPlayerOwner() {
        return sPlayerOwner;
    }

    public int getPcu() {
        return pcu;
    }

    public SFaction getFaction() {
        return faction;
    }
}
