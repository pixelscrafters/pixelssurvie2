package fr.pixelscrafters.survie.utils.inventory;

import fr.pixelscrafters.survie.utils.items.ItemFactory;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.Inventory;

import java.util.List;
import java.util.UUID;

public class InventoryUtils {

    public static Inventory createHeadInventory(String name, String inventoryIdentifier, List<UUID> players){
        Inventory inventory = Bukkit.createInventory(null,
                calculateInventorySize(players.size() + 1), name);
        inventory.setItem(0, ItemFactory.INSTANCE.createItem(Material.STONE_SWORD,
                inventoryIdentifier, "§lDans certains cas, des actions sont possibles :",
                "Clic gauche : §aOui", "Clic droit : §cNon"));
        for(UUID uuid : players){
            OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
            inventory.addItem(ItemFactory.INSTANCE.createHead(player));
        }
        return inventory;
    }

    public static int calculateInventorySize(int itemCount){
        return 9 * ((itemCount / 9) + 1);
    }
}
