package fr.pixelscrafters.survie.commands;

import fr.pixelscrafters.survie.managers.RankingManager;
import fr.pixelscrafters.survie.managers.exceptions.AlreadyExistsException;
import fr.pixelscrafters.survie.objects.ranking.RankingHologram;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AdminRankCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender.hasPermission("surv.admin.ranks")){
            if(sender instanceof Player) {
                Player p = (Player) sender;
                if(args.length == 0){
                    //Liste des hologrames
                } else if(args[0].equals("add") && args.length == 3){
                    //Ajout à l'endroit d'où est le joueur avec un nom.
                    RankingHologram.RankingType type = RankingHologram.RankingType.valueOf(args[2]);
                    if(type == null){
                        sender.sendMessage("§cType invalide.");
                    }else{
                        try{
                            RankingManager.INSTANCE.addNewRankingHologram(p.getLocation(), args[1], type);
                        }catch (AlreadyExistsException ex){
                            sender.sendMessage("§cUn holograme existe déjà avec ce nom.");
                        }
                    }
                } else if(args[0].equals("remove") && args.length == 2){
                    //Suppression avec le nom
                    RankingManager.INSTANCE.removeRankingHologram(args[1]);
                } else {
                    //Commande invalide
                    sender.sendMessage("§d/arank : §5Liste des hologrames");
                    sender.sendMessage("§d/arank add [NOM] [TYPE] : §5Ajoute un holograme");
                    sender.sendMessage("§7Types : §lPOWER, BLOCS, ONLINE, MONEY, ACHIEVEMENTS");
                    sender.sendMessage("§d/arank delete [NOM] : §5Supprime un holograme");

                }
            }else{
                sender.sendMessage("§cSeul un joueur peut effectuer cette commande.");
            }
        }else {
            sender.sendMessage("§cVous n'avez pas la permission d'effectuer cette commande.");
        }
        return true;
    }
}
