package fr.pixelscrafters.survie.managers;

import fr.pixelscrafters.survie.objects.factions.SFaction;
import fr.pixelscrafters.survie.objects.players.SPlayer;
import fr.pixelscrafters.survie.utils.persistance.IDataAdapter;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Bukkit.class})
public class TestFactionManager {

    @Mock
    IDataAdapter dataAdapter;

    @Mock
    Logger logger;

    @Mock
    Server server;

    @Captor
    ArgumentCaptor<String> stringArgumentCaptor;

    @Mock
    SFaction fac1;

    @Mock
    SFaction fac2;

    @Mock
    SPlayer sPlayer;

    @Test
    public void testLoadJsonError() throws Exception {
        when(dataAdapter.getTextData(any(String.class))).thenReturn("{}");
        PowerMockito.mockStatic(Bukkit.class);

        PowerMockito.when(Bukkit.class, "getLogger").thenReturn(logger);
        PowerMockito.when(Bukkit.class, "getServer").thenReturn(server);

        FactionManager fm = new FactionManager();
        fm.load(dataAdapter);

        Mockito.verify(server, times(1)).shutdown();
        PowerMockito.verifyStatic();

        assertFalse(fm.getFlagLoadedSuccess());
    }

    @Test
    public void testLoadJsonPass() throws Exception {
        when(dataAdapter.getTextData(any(String.class))).thenReturn("[{},{}]");
        PowerMockito.mockStatic(Bukkit.class);

        PowerMockito.when(Bukkit.class, "getLogger").thenReturn(logger);
        PowerMockito.when(Bukkit.class, "getServer").thenReturn(server);

        FactionManager fm = new FactionManager();
        fm.load(dataAdapter);

        Mockito.verify(server, times(0)).shutdown();
        PowerMockito.verifyStatic();

        assertTrue(fm.getFlagLoadedSuccess());
        assertEquals(2, fm.getSFactions().size());
    }

    @Test
    public void testAfterSuccessSaveSuccess() throws Exception {
        when(dataAdapter.getTextData(any(String.class))).thenReturn("[{},{}]");
        PowerMockito.mockStatic(Bukkit.class);

        PowerMockito.when(Bukkit.class, "getLogger").thenReturn(logger);
        PowerMockito.when(Bukkit.class, "getServer").thenReturn(server);

        FactionManager fm = new FactionManager();
        fm.load(dataAdapter);
        fm.save(dataAdapter);
        assertTrue(fm.getFlagLoadedSuccess());
        assertEquals(2, fm.getSFactions().size());

        Mockito.verify(server, times(0)).shutdown();
        Mockito.verify(dataAdapter, times(1)).writeTextData(anyString(),
                stringArgumentCaptor.capture());
        assertEquals("[{\"money\":0,\"plusMoneyQuest\":0,\"plusMoneyShop\":0,\"plusMoneyOthers\":0,\"lessMoneyShop\":0,\"lessMoneyOthers\":0},{\"money\":0,\"plusMoneyQuest\":0,\"plusMoneyShop\":0,\"plusMoneyOthers\":0,\"lessMoneyShop\":0,\"lessMoneyOthers\":0}]", stringArgumentCaptor.getValue());
        PowerMockito.verifyStatic();
    }

    @Test
    public void testFailSuccessSaveError() throws Exception {
        when(dataAdapter.getTextData(any(String.class))).thenReturn("{}");
        PowerMockito.mockStatic(Bukkit.class);

        PowerMockito.when(Bukkit.class, "getLogger").thenReturn(logger);
        PowerMockito.when(Bukkit.class, "getServer").thenReturn(server);

        FactionManager fm = new FactionManager();
        fm.load(dataAdapter);
        fm.save(dataAdapter);
        assertFalse(fm.getFlagLoadedSuccess());
        assertEquals(0, fm.getSFactions().size());

        Mockito.verify(server, times(1)).shutdown();
        Mockito.verify(dataAdapter, times(0)).writeTextData(anyString(), anyString());

        Mockito.verify(logger, atLeast(1)).warning("Cannot save faction data : An error " +
                "occurred while loading them," +
                " please fix this before trying to start your server.");

        PowerMockito.verifyStatic();
    }

    @Test
    public void testAddFactionAndFind(){
        FactionManager fm = new FactionManager();
        //FAC1
        UUID uuid1 = UUID.randomUUID();
        //FAC2
        UUID uuid2 = UUID.randomUUID();
        //FAC3 -> Not found
        UUID uuid3 = UUID.randomUUID();

        when(fac1.getUuid()).thenReturn(uuid1);
        when(fac2.getUuid()).thenReturn(uuid2);

        fm.addSFaction(fac1);
        fm.addSFaction(fac2);

        assertEquals(fac1, fm.getByUUID(uuid1));
        assertEquals(fac2, fm.getByUUID(uuid2));
        assertNull(fm.getByUUID(uuid3));
    }

    @Test
    public void testAddFactionAndFindFromPlayerUUIDMember(){
        FactionManager fm = new FactionManager();
        //FAC1
        UUID uuid1 = UUID.randomUUID();
        //FAC1 SPLAYER UUID NOT FOUND
        UUID uuid2 = UUID.randomUUID();
        //FAC1 SPLAYER UUID
        UUID uuid3 = UUID.randomUUID();

        when(fac1.getUuid()).thenReturn(uuid1);
        List<UUID> members = new ArrayList<>();
        members.add(uuid3);
        when(fac1.getMembers()).thenReturn(members);


        fm.addSFaction(fac1);

        assertNull(fm.getByPlayerUUID(uuid2));
        assertEquals(fac1, fm.getByPlayerUUID(uuid3));
    }

    @Test
    public void testAddFactionAndFindFromPlayerUUIDOperator(){
        FactionManager fm = new FactionManager();
        //FAC1
        UUID uuid1 = UUID.randomUUID();
        //FAC1 SPLAYER UUID NOT FOUND
        UUID uuid2 = UUID.randomUUID();
        //FAC1 SPLAYER UUID
        UUID uuid3 = UUID.randomUUID();

        when(fac1.getUuid()).thenReturn(uuid1);
        List<UUID> operators = new ArrayList<>();
        operators.add(uuid3);
        when(fac1.getOperators()).thenReturn(operators);


        fm.addSFaction(fac1);

        assertNull(fm.getByPlayerUUID(uuid2));
        assertEquals(fac1, fm.getByPlayerUUID(uuid3));
    }

    @Test
    public void testAddFactionAndFindFromPlayerOperator(){
        FactionManager fm = new FactionManager();
        //FAC1
        UUID uuid1 = UUID.randomUUID();
        //FAC1 SPLAYER UUID NOT FOUND
        UUID uuid2 = UUID.randomUUID();
        //FAC1 SPLAYER UUID
        UUID uuid3 = UUID.randomUUID();

        when(fac1.getUuid()).thenReturn(uuid1);
        when(sPlayer.getUuid()).thenReturn(uuid3);
        List<UUID> members = new ArrayList<>();
        members.add(uuid3);
        when(fac1.getOperators()).thenReturn(members);


        fm.addSFaction(fac1);

        assertEquals(fac1, fm.getBySPlayer(sPlayer));
    }

    @Test
    public void testAddFactionTwoTimes() throws Exception {
        PowerMockito.mockStatic(Bukkit.class);

        PowerMockito.when(Bukkit.class, "getLogger").thenReturn(logger);
        FactionManager fm = new FactionManager();
        //FAC1
        UUID uuid1 = UUID.randomUUID();

        when(fac1.getUuid()).thenReturn(uuid1);

        fm.addSFaction(fac1);
        fm.addSFaction(fac1);

        assertEquals(fac1, fm.getByUUID(uuid1));

        Mockito.verify(logger, atLeast(1)).warning("Something tried to add an already " +
                "existing faction.");

        PowerMockito.verifyStatic();

    }

    @Test
    public void testAddFactionAddAndRemove() throws Exception {
        PowerMockito.mockStatic(Bukkit.class);

        PowerMockito.when(Bukkit.class, "getLogger").thenReturn(logger);
        FactionManager fm = new FactionManager();
        //FAC1
        UUID uuid1 = UUID.randomUUID();

        when(fac1.getUuid()).thenReturn(uuid1);

        fm.addSFaction(fac1);
        fm.addSFaction(fac1);

        assertEquals(fac1, fm.getByUUID(uuid1));

        fm.removeSFaction(fac1);
        assertNull(fm.getByUUID(uuid1));

        PowerMockito.verifyStatic();

    }
}
