package fr.pixelscrafters.survie.objects.blocs;

public enum MapObjects {
    ASSEMBLER,
    SURVIVAL_KIT,
    SOLAR_PANEL,
}
