package fr.pixelscrafters.survie.utils.persistance;

public interface IDataAdapter {
    /**
     * This method allows you to read data from a location.
     * @param location Where the data is stored.
     * @return The data
     */
    public String getTextData(String location);

    /**
     * This method allows you to write data to a location.
     * @param location Where the data is stored.
     * @param text The text to write.
     */
    public void writeTextData(String location, String text);
}
