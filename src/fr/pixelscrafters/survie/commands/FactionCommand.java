package fr.pixelscrafters.survie.commands;

import fr.pixelscrafters.survie.managers.FactionManager;
import fr.pixelscrafters.survie.managers.SPlayerManager;
import fr.pixelscrafters.survie.objects.factions.SFaction;
import fr.pixelscrafters.survie.objects.players.SPlayer;
import fr.pixelscrafters.survie.utils.inventory.InventoryUtils;
import fr.pixelscrafters.survie.utils.items.ItemFactory;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class FactionCommand extends AbstractInventoryCommand {
    //MENU NAMES
    public static final String MENU_NAME = "Ma Faction";
    public static final String SUB_MENU_NO_FACTION = "Créer / Rejoindre une faction";
    public static final String SUB_MENU_HAS_FACTION = "Modifier ma faction";
    public static final String SUB_MENU_SELECT_FACTION = "Choisir une faction où postuler";
    public static final String SUB_MENU_ACCEPT_MEMBER = "Accepter ou refuser des membres";
    public static final String SUB_MENU_KICK_MEMBER = "Expulser des membres";
    public static final String SUB_MENU_RANK_MEMBER = "Grader un membre";
    public static final String SUB_MENU_UNRANK_OPERATOR = "Dégrader un opérateur";


    //MENUS IDENTIFIERS
    public static final int MENU_IDENTIFIER = 0;

    public static final int MENU_IDENTIFIER1 = 9 * 1 + 2;
    public static final int MENU_IDENTIFIER2 = 9 * 1 + 4;
    public static final int MENU_IDENTIFIER3 = 9 * 1 + 6;
    public static final int MENU_IDENTIFIER4 = 9 * 2 + 2;
    public static final int MENU_IDENTIFIER5 = 9 * 2 + 4;
    public static final int MENU_IDENTIFIER6 = 9 * 2 + 6;

    public static final int NEXT_PAGE_SLOT = 35;
    public static final int PREVIOUS_PAGE_SLOT = 34;
    public static final int FACTION_MAX_SLOT = 32;

    public FactionCommand() {
        super(MENU_NAME);
    }

    // This method is called, when somebody uses our command
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //Si pas de faction
        /*
        -> Menu Jouer sans faction
        -> Créer une faction
        -> Rejoindre une faction
         */

        //Si faction
        /*
        -> Menu accepter des membres
        -> Menu virer des membres
        -> Menu promouvoir des membres
        -> Menu rétrograder des membres
        -> Affichage argent de faction
        -> Supprimer la faction
         */
        if (sender instanceof Player) {
            Player p = (Player) sender;
            SPlayer sp = SPlayerManager.INSTANCE.getSPlayer(p);
            if(args.length == 0) {
                p.openInventory(createFactionMainMenu(sp));
            } else if(args[0].equals("rename")){
                SFaction faction = FactionManager.INSTANCE.getBySPlayer(sp);
                if(faction != null && faction.isOperator(sp)){
                    StringBuilder builder = new StringBuilder();
                    for(int i = 1; i < args.length; i++){
                        builder.append(args[i]);
                        if(i != args.length -1){
                            builder.append(" ");
                        }
                    }
                    faction.rename(builder.toString());
                    p.sendMessage("§dFaction renommée avec succès sur §5" + faction.getName());
                } else if(faction == null){
                    p.sendMessage("§cIl faudrait peut être avoir une faction avant ...");
                } else {
                    p.sendMessage("§4Vous n'êtes pas opérateur de cette faction.");
                }
            }
        } else {
            sender.sendMessage("Commande à effectuer en tant que joueur !");
        }
        return true;

    }

    private Inventory createFactionMainMenu(SPlayer sPlayer) {
        Inventory inventory = Bukkit.createInventory(null, 9 * 3, MENU_NAME);
        SFaction faction = FactionManager.INSTANCE.getBySPlayer(sPlayer);
        if (faction == null) {
            //Inventory identifier
            inventory.setItem(MENU_IDENTIFIER, ItemFactory.INSTANCE.createItem(Material.STONE_SWORD,
                    SUB_MENU_NO_FACTION));

            inventory.setItem(MENU_IDENTIFIER1, ItemFactory.INSTANCE.createItem(Material.STONE, "Jouer sans faction",
                    "§4Attention !", "Vous ne pourrez pas acheter aux shops", "tant que vous ne rejoignez pas",
                    "une faction."));

            inventory.setItem(MENU_IDENTIFIER2, ItemFactory.INSTANCE.createItem(Material.WRITABLE_BOOK,
                    "Créer une faction"));

            inventory.setItem(MENU_IDENTIFIER3, ItemFactory.INSTANCE.createItem(Material.WRITABLE_BOOK,
                    "Rejoindre une faction"));

        }
        else {
            //Inventory identifier
            inventory.setItem(MENU_IDENTIFIER, ItemFactory.INSTANCE.createItem(Material.STONE_SWORD,
                    SUB_MENU_HAS_FACTION));

            inventory.setItem(MENU_IDENTIFIER1, ItemFactory.INSTANCE.createItem(Material.MYCELIUM,
                    "Accepter / Refuser des membres",
                    "Affiche les joueurs qui veulent rejoindre votre faction."));

            inventory.setItem(MENU_IDENTIFIER2, ItemFactory.INSTANCE.createItem(Material.LAVA_BUCKET,
                    "Expulser des membres"));

            inventory.setItem(MENU_IDENTIFIER3, ItemFactory.INSTANCE.createItem(Material.GOLDEN_SHOVEL,
                    "Grader un membre", "Passer des membres en admin !",
                    "Attention, ils auront les mêmes permissions", "que vous.",
                    "§4§lAucun remboursement en cas de problème de cohésion de groupe."));

            inventory.setItem(MENU_IDENTIFIER4, ItemFactory.INSTANCE.createItem(Material.STONE_SHOVEL,
                    "Dégrader un membre", "Passer des admins en joueurs !"));

            inventory.setItem(MENU_IDENTIFIER5, ItemFactory.INSTANCE.createItem(Material.DIAMOND,
                    "Affichage du budget", "Affiche les stats monétaires"));

            inventory.setItem(MENU_IDENTIFIER6, ItemFactory.INSTANCE.createItem(Material.DIAMOND,
                    "§4§lSUPPRIMER LA FACTION", "Attention ! Pas de confirmation !",
                    "§4§lPas de remboursement en cas de mauvais click"));
        }

        return inventory;
    }

    private Inventory createFactionListMenu(int page){
        Inventory inventory = Bukkit.createInventory(null,9*4, MENU_NAME);
        //Inventory identifier
        inventory.setItem(MENU_IDENTIFIER, ItemFactory.INSTANCE.createItem(Material.STONE_SWORD,
                SUB_MENU_SELECT_FACTION));

        if(FactionManager.INSTANCE.getSFactions().size() - (page * FACTION_MAX_SLOT) > 0) {
            inventory.setItem(NEXT_PAGE_SLOT, ItemFactory.INSTANCE.createItem(Material.GREEN_DYE, "Suivant",
                    "Vers page : " + (page + 1)));
        }
        if(page != 1){
            inventory.setItem(PREVIOUS_PAGE_SLOT,ItemFactory.INSTANCE.createItem(Material.YELLOW_DYE,"Précédent",
                    "Vers page : " + (page-1)));
        }

        try {
            for (int i = (page - 1) * FACTION_MAX_SLOT; i <= (page - 1) * FACTION_MAX_SLOT + FACTION_MAX_SLOT; i++){
                SFaction sFaction = FactionManager.INSTANCE.getSFactions().get(i);
                inventory.addItem(ItemFactory.INSTANCE.createItem(Material.APPLE, sFaction.getName(),
                        sFaction.getUuid().toString()));
            }
        } catch (IndexOutOfBoundsException ex){

        }
        return inventory;
    }

    @Override
    public void onInventoryClick(InventoryClickEvent event) {
        event.setCancelled(true);
        ItemStack itemStack = event.getCurrentItem();
        ItemStack identifier = event.getInventory().getItem(0);
        if (identifier == null) {
            Bukkit.getLogger().warning("The targeted inventory has a missing item.");
            return;
        } else {
            switch (identifier.getItemMeta().getDisplayName()) {
                case SUB_MENU_NO_FACTION:
                    treatmentSubMenuNoFaction((Player) event.getWhoClicked(),
                            SPlayerManager.INSTANCE.getSPlayer(event.getWhoClicked().getUniqueId()),
                            itemStack, event.getSlot());
                    break;
                case SUB_MENU_HAS_FACTION:
                    treatmentSubMenuHasFaction((Player) event.getWhoClicked(),
                            SPlayerManager.INSTANCE.getSPlayer(event.getWhoClicked().getUniqueId()),
                            itemStack, event.getSlot());
                    break;
                case SUB_MENU_SELECT_FACTION:
                    treatmentSubMenuFactionList((Player) event.getWhoClicked(),
                            SPlayerManager.INSTANCE.getSPlayer(event.getWhoClicked().getUniqueId()),
                            itemStack, event.getSlot());
                    break;
                case SUB_MENU_ACCEPT_MEMBER:
                    treatmentSubMenuAcceptMember((Player) event.getWhoClicked(),
                            SPlayerManager.INSTANCE.getSPlayer(event.getWhoClicked().getUniqueId()),
                            itemStack, event.getSlot(), event.isLeftClick());
                    break;
                case SUB_MENU_KICK_MEMBER:
                    treatmentSubMenuKickMember((Player) event.getWhoClicked(),
                            SPlayerManager.INSTANCE.getSPlayer(event.getWhoClicked().getUniqueId()),
                            itemStack, event.getSlot());
                    break;
                case SUB_MENU_RANK_MEMBER:
                    treatmentSubMenuRankMember((Player) event.getWhoClicked(),
                            SPlayerManager.INSTANCE.getSPlayer(event.getWhoClicked().getUniqueId()),
                            itemStack, event.getSlot());
                    break;
                case SUB_MENU_UNRANK_OPERATOR:
                    treatmentSubMenuUnRankMember((Player) event.getWhoClicked(),
                            SPlayerManager.INSTANCE.getSPlayer(event.getWhoClicked().getUniqueId()),
                            itemStack, event.getSlot());
                    break;
            }
        }
    }

    private void treatmentSubMenuNoFaction(Player p, SPlayer sPlayer, ItemStack clickedItem, int slot) {
        if (slot == MENU_IDENTIFIER1) {
            p.closeInventory();
            p.sendTitle("§4Vous n'avez pas de faction", "§dVous ne pouvez pas utiliser les shops.",
                    10, 100, 10);
            p.sendMessage("§5Faites /faction pour ouvrir le menu faction.");
        } else if (slot == MENU_IDENTIFIER2) {
            SFaction sFaction = new SFaction("Faction de" + p.getDisplayName(), sPlayer);
            p.closeInventory();
            FactionManager.INSTANCE.addSFaction(sFaction);
            p.sendTitle("§aVotre faction a été créée avec succès !",
                    "Modifiez le nom en faisant §l/faction rename [NOM]", 10, 100, 10);

        } else if (slot == MENU_IDENTIFIER3) {
            //Todo afficher les factions
            p.openInventory(createFactionListMenu(1));
        }
    }

    private void treatmentSubMenuHasFaction(Player p, SPlayer sPlayer, ItemStack clickedItem, int slot) {
        SFaction faction = FactionManager.INSTANCE.getBySPlayer(sPlayer);
        if(slot != MENU_IDENTIFIER5 && faction != null && !faction.isOperator(sPlayer)){
            p.stopSound(Sound.ENTITY_ENDER_DRAGON_GROWL);
            p.closeInventory();
            p.sendTitle("§4IMPOSSIBBBRUHH","§cVous n'êtes pas admin de la faction", 10, 100, 10);
            return;
        }
        if (slot == MENU_IDENTIFIER1) {
            p.openInventory(InventoryUtils.createHeadInventory(MENU_NAME, SUB_MENU_ACCEPT_MEMBER,
                    faction.getWantsToJoin()));
        } else if (slot == MENU_IDENTIFIER2) {
            p.openInventory(InventoryUtils.createHeadInventory(MENU_NAME, SUB_MENU_KICK_MEMBER,
                    faction.getMembers()));
        } else if (slot == MENU_IDENTIFIER3) {
            p.openInventory(InventoryUtils.createHeadInventory(MENU_NAME, SUB_MENU_RANK_MEMBER,
                    faction.getMembers()));
        } else if(slot == MENU_IDENTIFIER4) {
            p.openInventory(InventoryUtils.createHeadInventory(MENU_NAME, SUB_MENU_UNRANK_OPERATOR,
                    faction.getOperators().stream()
                            .filter(x-> !x.equals(sPlayer.getUuid())).collect(Collectors.toList())));
        } else if (slot == MENU_IDENTIFIER5) {
            p.openBook(createMoneyBook(faction));
        } else if (slot == MENU_IDENTIFIER6) {
            FactionManager.INSTANCE.removeSFaction(faction);
            p.sendMessage("§aOpération réussie, Mais... Vous êtes Sans Faction !");
            p.closeInventory();
        }
    }

    private void treatmentSubMenuAcceptMember(Player p, SPlayer sPlayer, ItemStack clickedItem,
                                              int slot, boolean accepted) {
        if(clickedItem != null && slot != 0){
            SFaction faction = FactionManager.INSTANCE.getBySPlayer(sPlayer);
            //Its a player head !!
            SkullMeta meta = (SkullMeta) clickedItem.getItemMeta();
            OfflinePlayer player = meta.getOwningPlayer();
            if(accepted) {
                faction.getMembers().add(player.getUniqueId());
                faction.removeWantsToJoin(player.getUniqueId());
                if (player.isOnline()) {
                    Player onlinePlayer = (Player) player;
                    onlinePlayer.sendTitle("§5" + p.getDisplayName() + " §5vous a accepté !", "§5Bienvenue chez " +
                            "§d" + faction.getName(), 10, 100, 10);
                }
            }else {
                faction.removeWantsToJoin(player.getUniqueId());
                if (player.isOnline()) {
                    Player onlinePlayer = (Player) player;
                    onlinePlayer.sendTitle("§c" + p.getDisplayName() + " §4vous a refusé !", "",
                            10, 100, 10);
                }
            }
            treatmentSubMenuHasFaction(p, sPlayer, clickedItem, MENU_IDENTIFIER1);
        }
    }

    private void treatmentSubMenuKickMember(Player p, SPlayer sPlayer, ItemStack clickedItem, int slot) {
        if(clickedItem != null && slot != 0) {
            SFaction faction = FactionManager.INSTANCE.getBySPlayer(sPlayer);
            //Its a player head !!
            SkullMeta meta = (SkullMeta) clickedItem.getItemMeta();
            OfflinePlayer player = meta.getOwningPlayer();
            faction.removeMember(SPlayerManager.INSTANCE.getSPlayer(player.getUniqueId()));
            if (player.isOnline()) {
                Player onlinePlayer = (Player) player;
                onlinePlayer.sendTitle("§c" + p.getDisplayName() + " §4vous a expulsé !", "§4Vous n'êtes " +
                        "plus chez §c" + faction.getName(), 10, 100, 10);
            }
            treatmentSubMenuHasFaction(p, sPlayer, clickedItem, MENU_IDENTIFIER2);
        }
    }

    private void treatmentSubMenuRankMember(Player p, SPlayer sPlayer, ItemStack clickedItem, int slot) {
        if(clickedItem != null && slot != 0) {
            SFaction faction = FactionManager.INSTANCE.getBySPlayer(sPlayer);
            //Its a player head !!
            SkullMeta meta = (SkullMeta) clickedItem.getItemMeta();
            OfflinePlayer player = meta.getOwningPlayer();
            faction.addOperator(SPlayerManager.INSTANCE.getSPlayer(player.getUniqueId()));
            if (player.isOnline()) {
                Player onlinePlayer = (Player) player;
                onlinePlayer.sendTitle("§5" + p.getDisplayName() + " §5vous a gradé !", "§5Vous êtes " +
                        "opérateur de §d" + faction.getName(), 10, 100, 10);
            }
            treatmentSubMenuHasFaction(p, sPlayer, clickedItem, MENU_IDENTIFIER2);
        }
    }

    private void treatmentSubMenuUnRankMember(Player p, SPlayer sPlayer, ItemStack clickedItem, int slot) {
        if(clickedItem != null && slot != 0) {
            SFaction faction = FactionManager.INSTANCE.getBySPlayer(sPlayer);
            //Its a player head !!
            SkullMeta meta = (SkullMeta) clickedItem.getItemMeta();
            OfflinePlayer player = meta.getOwningPlayer();
            faction.removeOperator(SPlayerManager.INSTANCE.getSPlayer(player.getUniqueId()));
            if (player.isOnline()) {
                Player onlinePlayer = (Player) player;
                onlinePlayer.sendTitle("§c" + p.getDisplayName() + " §4vous a dégradé !", "§4Vous n'êtes " +
                        "plus opérateur de §c" + faction.getName(), 10, 100, 10);
            }
            treatmentSubMenuHasFaction(p, sPlayer, clickedItem, MENU_IDENTIFIER4);
        }
    }

    private void treatmentSubMenuFactionList(Player p, SPlayer sPlayer, ItemStack clickedItem, int slot) {
        if(slot == (9*4)-1 && clickedItem != null){
            List<String> lore = clickedItem.getItemMeta().getLore();
            String line = lore.get(0);
            int pageNumber = Integer.parseInt(line.substring(12));
            p.openInventory(createFactionListMenu(pageNumber));
        } else if(slot == (9*4)-2 && clickedItem != null){
            List<String> lore = clickedItem.getItemMeta().getLore();
            String line = lore.get(0);
            int pageNumber = Integer.parseInt(line.substring(12));
            p.openInventory(createFactionListMenu(pageNumber));
        } else if(clickedItem != null && slot != 0){
            List<String> lore = clickedItem.getItemMeta().getLore();
            String line = lore.get(0);
            SFaction faction = FactionManager.INSTANCE.getByUUID(UUID.fromString(line));
            if(faction == null){
                Bukkit.getLogger().warning("Player tried to join faction with uuid : '" + line +
                        "' but this faction wasn't found.");
                return;
            }
            faction.addWantsToJoin(sPlayer);
            p.closeInventory();
            p.sendMessage("§aDemande envoyée !");

            //Notification des opérateurs de faction
            for(UUID uuid : faction.getOperators()){

                Player operator = Bukkit.getPlayer(uuid);
                if(operator != null){
                    operator.sendMessage("§5Le joueur §d" + p.getDisplayName() + " §5veut rejoindre votre faction.");
                }
            }
        }
    }

    private ItemStack createMoneyBook(SFaction faction){
        ItemStack is = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta b = (BookMeta) is.getItemMeta();
        b.setAuthor("§a" + faction.getName());
        b.addPage("§5---\n§dEtat des comptes\n§5---\n\n§5Total : §d" + faction.getMoney(),
                "§5Dépenses Shops :§d" + faction.getLessMoneyShop() + "\n§5Dépenses Autres :§d"
                        + faction.getLessMoneyOthers(),
                "§5Gains Quêtes :§d" + faction.getPlusMoneyQuest() + "\n§5Gains Shops :§d" + faction.getPlusMoneyShop() +
                        "\n§5Gains Autres :§d" + faction.getPlusMoneyOthers()
        );
        b.setTitle("Blank");
        is.setItemMeta(b);
        return is;
    }


}