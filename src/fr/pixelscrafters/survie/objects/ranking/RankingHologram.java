package fr.pixelscrafters.survie.objects.ranking;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.line.TextLine;
import com.google.gson.annotations.Expose;
import fr.pixelscrafters.survie.main.PluginMain;
import fr.pixelscrafters.survie.managers.FactionManager;
import fr.pixelscrafters.survie.objects.factions.SFaction;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class RankingHologram {
    public static enum RankingType {
        POWER{
            @Override
            public String getName() {
                return "Les plus puissantes";
            }

            @Override
            public void update(Hologram holo) {
                List<SFaction> factions = FactionManager.INSTANCE.getSFactions();
                factions.sort(new Comparator<SFaction>() {
                    @Override
                    public int compare(SFaction o1, SFaction o2) {
                        return 0;
                    }
                });
                //Todo implement faction Power Definition.
            }
        }, BLOCS{
            @Override
            public String getName() {
                return "Le plus de blocs";
            }

            @Override
            public void update(Hologram holo) {
                //Todo implement faction blocs definition
            }
        }, ONLINE{
            @Override
            public String getName() {
                return "En ligne";
            }

            @Override
            public void update(Hologram holo) {
                //Copy of list of factions
                List<SFaction> factions = new ArrayList<>(FactionManager.INSTANCE.getSFactions());
                factions.sort(new Comparator<SFaction>() {
                    @Override
                    public int compare(SFaction o1, SFaction o2) {
                        long countO1 = o1.countOnlinePlayers();
                        long countO2 = o2.countOnlinePlayers();
                        if(countO1 > countO2){
                            return 1;
                        }else if(countO1 == countO2){
                            return 0;
                        }else{
                            return -1;
                        }
                    }
                });
                int max = factions.size() >= 10 ? 10 : factions.size();
                for(int i = 0; i< max; i++){
                    ((TextLine)holo.getLine(3 + i)).setText("§5"+ (i+1) + ": §d" + factions.get(i).getName() +
                            " -> §7" + factions.get(i).countOnlinePlayers());
                }
            }
        }, MONEY{
            @Override
            public String getName() {
                return "Les plus riches";
            }

            @Override
            public void update(Hologram holo) {
                List<SFaction> factions = new ArrayList<>(FactionManager.INSTANCE.getSFactions());
                factions.sort(new Comparator<SFaction>() {
                    @Override
                    public int compare(SFaction o1, SFaction o2) {
                        long countO1 = o1.getMoney();
                        long countO2 = o2.getMoney();
                        if(countO1 > countO2){
                            return 1;
                        }else if(countO1 == countO2){
                            return 0;
                        }else{
                            return -1;
                        }
                    }
                });
                int max = factions.size() >= 10 ? 10 : factions.size();
                for(int i = 0; i< max; i++){
                    ((TextLine)holo.getLine(3 + i)).setText("§5"+ (i+1) + ": §d" + factions.get(i).getName() +
                            " -> §7" + factions.get(i).getMoney());
                }
            }
        }, ACHIEVEMENTS{
            @Override
            public String getName() {
                return "Ayant le plus d'achievements";
            }

            @Override
            public void update(Hologram holo) {
                //Todo with achievement system.
            }
        };

        public abstract String getName();
        public abstract void update(Hologram holo);
    }

    private String name;
    private int x;
    private int y;
    private int z;
    private String worldName;
    private RankingType type;

    @Expose(serialize = false, deserialize = false)
    private transient Hologram holo;

    public String getName() {
        return name;
    }

    public RankingHologram(String name, int x, int y, int z, String worldName, RankingType type) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.z = z;
        this.worldName = worldName;
        this.type = type;
    }

    public void createHologram(){
        Location l = new Location(Bukkit.getWorld(worldName), x, y , z);
        Hologram hologram = HologramsAPI.createHologram(PluginMain.getInstance(), l);
        hologram.appendTextLine("§5-------------------------------");
        hologram.appendTextLine("§5-- §d" + type.getName() + " §5--");
        hologram.appendTextLine("§5-------------------------------");
        //Factions
        hologram.appendTextLine("§51: §7-*-*-*-");   //1
        hologram.appendTextLine("§52: §7-*-*-*-");   //2
        hologram.appendTextLine("§53: §7-*-*-*-");   //3
        hologram.appendTextLine("§54: §7-*-*-*-");   //4
        hologram.appendTextLine("§55: §7-*-*-*-");   //5
        hologram.appendTextLine("§56: §7-*-*-*-");   //6
        hologram.appendTextLine("§57: §7-*-*-*-");   //7
        hologram.appendTextLine("§58: §7-*-*-*-");   //8
        hologram.appendTextLine("§59: §7-*-*-*-");   //9
        hologram.appendTextLine("§510: §7-*-*-*-");   //10
        this.holo = hologram;
        update();
    }

    public void update(){
        this.type.update(holo);
    }

    public void destroy(){
        this.holo.delete();
    }
}
