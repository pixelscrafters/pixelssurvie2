package fr.pixelscrafters.survie.listeners;

import fr.pixelscrafters.survie.commands.AbstractInventoryCommand;
import fr.pixelscrafters.survie.commands.AdminFactionCommand;
import fr.pixelscrafters.survie.main.PluginMain;
import fr.pixelscrafters.survie.managers.FactionManager;
import fr.pixelscrafters.survie.objects.factions.SFaction;
import fr.pixelscrafters.survie.objects.players.SPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class InventoryListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event){
        InventoryView inventoryView = event.getView();
        AbstractInventoryCommand command = PluginMain.findCommand(inventoryView.getTitle());
        if(command != null){
            command.onInventoryClick(event);
        }
    }

}
