package fr.pixelscrafters.survie.objects.players;

import org.bukkit.entity.Player;

import java.util.UUID;

public class SPlayer {
    private UUID uuid;

    public SPlayer(Player p){
        this.uuid = p.getUniqueId();
    }

    public UUID getUuid() {
        return uuid;
    }
}
