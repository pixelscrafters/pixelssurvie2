package fr.pixelscrafters.survie.managers.exceptions;

public class AlreadyExistsException extends Exception{
    public AlreadyExistsException(Object item) {
        super("The specified item already exists : " + item.toString());
    }
}
