package fr.pixelscrafters.survie.listeners;

import fr.pixelscrafters.survie.managers.SPlayerManager;
import fr.pixelscrafters.survie.managers.exceptions.AlreadyExistsException;
import fr.pixelscrafters.survie.objects.players.SPlayer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        try{
            SPlayerManager.INSTANCE.addPlayer(new SPlayer(event.getPlayer()));
            Bukkit.broadcastMessage("§d" + event.getPlayer().getDisplayName() + " §5a rejoint le serveur pour la 1ere fois");
;        }catch(AlreadyExistsException ex){
            //Le joueur existe déjà
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event){

    }
}
