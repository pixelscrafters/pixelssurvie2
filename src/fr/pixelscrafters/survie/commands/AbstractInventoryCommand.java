package fr.pixelscrafters.survie.commands;

import org.bukkit.command.CommandExecutor;
import org.bukkit.event.inventory.InventoryClickEvent;

public abstract class AbstractInventoryCommand implements CommandExecutor {
    private String name;

    public String getName() {
        return name;
    }

    public AbstractInventoryCommand(String name) {
        this.name = name;
    }
    public abstract void onInventoryClick(InventoryClickEvent event);
}
