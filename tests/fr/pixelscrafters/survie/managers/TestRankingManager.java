package fr.pixelscrafters.survie.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.pixelscrafters.survie.objects.ranking.RankingHologram;
import org.junit.Test;

import java.util.ArrayList;

public class TestRankingManager {
    @Test
    public void test(){
        RankingHologram hg = new RankingHologram("a", 0,0,0, "aa", RankingHologram.RankingType.MONEY);
        RankingHologram hg2 = new RankingHologram("a", 0,0,0, "aa", RankingHologram.RankingType.MONEY);
        ArrayList<RankingHologram> holos = new ArrayList<>();
        holos.add(hg);
        holos.add(hg2);
        Gson gson = new GsonBuilder().create();
        System.out.println(gson.toJson(holos));
    }
}
