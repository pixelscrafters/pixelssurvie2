package fr.pixelscrafters.survie.main;

import fr.pixelscrafters.survie.commands.AdminRankCommand;
import fr.pixelscrafters.survie.commands.FactionCommand;
import fr.pixelscrafters.survie.commands.AdminFactionCommand;
import fr.pixelscrafters.survie.commands.AbstractInventoryCommand;
import fr.pixelscrafters.survie.listeners.InventoryListener;
import fr.pixelscrafters.survie.listeners.PlayerListener;
import fr.pixelscrafters.survie.managers.FactionManager;
import fr.pixelscrafters.survie.managers.IManager;
import fr.pixelscrafters.survie.managers.RankingManager;
import fr.pixelscrafters.survie.managers.SPlayerManager;
import fr.pixelscrafters.survie.utils.persistance.DiskData;
import fr.pixelscrafters.survie.utils.persistance.IDataAdapter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * Main du Plugin.
 */
public class PluginMain extends JavaPlugin {
    private IManager managers[] = {SPlayerManager.INSTANCE, FactionManager.INSTANCE, RankingManager.INSTANCE};
    private IDataAdapter dataAdapter;
    private static ArrayList<AbstractInventoryCommand> registeredCommand = new ArrayList<>();
    private static PluginMain instance;

    private static void registerInventoryCommand(String name, AbstractInventoryCommand command){
        instance.getCommand(name).setExecutor(command);
        registeredCommand.add(command);
    }

    public static AbstractInventoryCommand findCommand(String inventoryName){
        try {
            return registeredCommand.stream().filter(x-> x.getName().equals(inventoryName)).findFirst().get();
        } catch (NoSuchElementException ex){
            return null;
        }

    }

    public static PluginMain getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        Bukkit.getLogger().info("§5Starting PixelsSurvie ...");
        Bukkit.getLogger().info("§5Defining default data manager ...");
        setDataAdapter();

        Bukkit.getLogger().info("§5Registering commands");
        registerInventoryCommand("afaction", new AdminFactionCommand());
        registerInventoryCommand("faction", new FactionCommand());
        this.getCommand("arank").setExecutor(new AdminRankCommand());

        Bukkit.getLogger().info("§5Loading all data ...");
        for(IManager manager : managers) {
            manager.load(getDataAdapter());
        }

        Bukkit.getLogger().info("§5Registering events");
        getServer().getPluginManager().registerEvents(new InventoryListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerListener(), this);

        Bukkit.getLogger().info("§5Done !");

    }

    @Override
    public void onDisable() {
        for(IManager manager : managers) {
            manager.save(getDataAdapter());
        }
    }

    /**
     * Method thread safe in order to get the data adapter.
     * @return The linked data adapter.
     */
    public synchronized IDataAdapter getDataAdapter(){
        return dataAdapter;
    }

    public synchronized void setDataAdapter(){
        this.dataAdapter = new DiskData();
    }
}