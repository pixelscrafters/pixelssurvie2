package fr.pixelscrafters.survie.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import fr.pixelscrafters.survie.managers.exceptions.AlreadyExistsException;
import fr.pixelscrafters.survie.objects.players.SPlayer;
import fr.pixelscrafters.survie.utils.persistance.IDataAdapter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class allows us to save an read stored data about players.
 */
public class SPlayerManager implements IManager{
    private static final String PLAYERS_DATA_LOCATION = "plugins/PixelsSurvie/players.json";
    public static final SPlayerManager INSTANCE = new SPlayerManager();

    private List<SPlayer> players = new ArrayList<>();

    private ReentrantLock lock = new ReentrantLock();
    private boolean flagLoadedSuccessfully = false;

    private SPlayerManager(){

    }

    @Override
    public void load(IDataAdapter dataAdapter){
        //LOADING DATA
        lock.lock();
        Bukkit.getLogger().info("Loading player data...");
        String jsonRaw = dataAdapter.getTextData(PLAYERS_DATA_LOCATION);
        try {
            Gson gson = new GsonBuilder().create();
            players = gson.fromJson(jsonRaw, new TypeToken<List<SPlayer>>() {
            }.getType());
            flagLoadedSuccessfully = true;
            Bukkit.getLogger().info("Player data loaded.");
            if(players != null) {
                Bukkit.getLogger().info("Loaded " + players.size() + " value(s).");
            } else {
                Bukkit.getLogger().info("The list of players seems to be empty ...");
                players = new ArrayList<>();
            }
        } catch (JsonParseException ex){
            Bukkit.getLogger().severe("Error when loading player data !\n\n" + ex.getMessage());
            Bukkit.getLogger().severe("§4§lServer will be switched off now !");
            Bukkit.getServer().shutdown();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void save(IDataAdapter dataAdapter){
        //SAVING DATA
        lock.lock();
        try {
            Bukkit.getLogger().info("Saving players data...");
            if (flagLoadedSuccessfully) {
                Gson gson = new GsonBuilder().create();
                dataAdapter.writeTextData(PLAYERS_DATA_LOCATION, gson.toJson(players));
                Bukkit.getLogger().info("Saved successfully !");
            } else {
                Bukkit.getLogger().warning("Cannot save player data : An error occurred while loading them," +
                        " please fix this before trying to start your server.");
            }
        }finally {
            lock.unlock();
        }
    }

    /**
     * This method is used in order to add new players to the database. Only use it when a NEW player joins the server.
     * @param sPlayer The new player
     * @throws AlreadyExistsException When a SPlayer is already present.
     */
    public void addPlayer(SPlayer sPlayer) throws AlreadyExistsException {
        if(!exists(sPlayer)){
            players.add(sPlayer);
        } else {
            throw new AlreadyExistsException(sPlayer);
        }
    }

    /**
     * This method is used in order to check if a player already exists.
     * @param sPlayer The player to check.
     * @return true if the player exists, false if not.
     */
    public boolean exists(SPlayer sPlayer){
        return getSPlayer(sPlayer.getUuid()) != null;
    }

    /**
     * This method is used in order to get an SPlayer from an UUID
     * @param uuid The identifier of the SPlayer. It has to be linked to a real player
     * @return The SPlayer registered, null if the UUID is not found.
     */
    public SPlayer getSPlayer(UUID uuid){
        try{
            return players.stream().filter(x -> x.getUuid().equals(uuid)).findFirst().get();
        }catch (NoSuchElementException ex){
            return null;
        }
    }

    /**
     * This method is used in order to get an SPlayer from a Player.
     * @param player The player (online) linked to the SPlayer.
     * @return the SPlayer registered.
     */
    public SPlayer getSPlayer(Player player){
        return getSPlayer(player.getUniqueId());
    }
}
