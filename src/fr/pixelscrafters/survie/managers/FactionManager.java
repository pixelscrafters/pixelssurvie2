package fr.pixelscrafters.survie.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import fr.pixelscrafters.survie.objects.factions.SFaction;
import fr.pixelscrafters.survie.objects.players.SPlayer;
import fr.pixelscrafters.survie.utils.persistance.IDataAdapter;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

public class FactionManager implements IManager {
    private static final String FACTION_DATA_LOCATION = "plugins/PixelsSurvie/factions.json";
    public static final FactionManager INSTANCE = new FactionManager();

    private ReentrantLock lock = new ReentrantLock();
    private List<SFaction> factions;
    private boolean flagLoadedSuccessfully = false;

    protected FactionManager(){
        this.factions = new ArrayList<>();
    }

    @Override
    public void load(IDataAdapter dataAdapter) {
        //LOADING DATA
        lock.lock();
        Bukkit.getLogger().info("Loading faction data...");
        String jsonRaw = dataAdapter.getTextData(FACTION_DATA_LOCATION);
        try {
            Gson gson = new GsonBuilder().create();
            factions = gson.fromJson(jsonRaw, new TypeToken<List<SFaction>>() {
            }.getType());
            flagLoadedSuccessfully = true;
            Bukkit.getLogger().info("Faction data loaded.");
            if(factions != null) {
                Bukkit.getLogger().info("Loaded " + factions.size() + " value(s).");
            } else {
                Bukkit.getLogger().info("The list of factions seems to be empty ...");
                factions = new ArrayList<>();
            }
        } catch (JsonParseException ex){
            Bukkit.getLogger().severe("Error when loading faction data !\n\n" + ex.getMessage());
            Bukkit.getLogger().severe("§4§lServer will be switched off now !");
            Bukkit.getServer().shutdown();
        } finally {
            lock.unlock();
        }

    }

    @Override
    public void save(IDataAdapter dataAdapter) {
        //SAVING DATA
        lock.lock();
        try {
            Bukkit.getLogger().info("Saving faction data...");
            if (flagLoadedSuccessfully) {
                Gson gson = new GsonBuilder().create();
                dataAdapter.writeTextData(FACTION_DATA_LOCATION, gson.toJson(factions));
                Bukkit.getLogger().info("Saved successfully !");
            } else {
                Bukkit.getLogger().warning("Cannot save faction data : An error occurred while loading them," +
                        " please fix this before trying to start your server.");
            }
        }finally {
            lock.unlock();
        }
    }

    /**
     * Method used in order to know if the faction data has been loaded.
     * @return if the data is loaded.
     */
    public final boolean getFlagLoadedSuccess(){
        return flagLoadedSuccessfully;
    }

    /**
     * Method used in order to get the list of existing factions.
     * @return The list of all factions.
     */
    public final List<SFaction> getSFactions(){
        return factions;
    }

    /**
     * Gets a faction by its own UUID.
     * @param factionUUID The UUID of the targeted faction.
     * @return The faction, null if not found.
     */
    public SFaction getByUUID(UUID factionUUID) {
        try{
            lock.lock();
            Optional<SFaction> optionalSFaction = factions.stream()
                    .filter(x -> x.getUuid().equals(factionUUID)).findFirst();
            return optionalSFaction.orElse(null);
        }finally {
            lock.unlock();
        }
    }


    /**
     * Gets a faction by members or operator UUID.
     * @param playerUUID The member or operator of the faction.
     * @return The faction, null if not found.
     */
    public SFaction getByPlayerUUID(UUID playerUUID){
        try{
            lock.lock();
            Optional<SFaction> optionalSFaction = factions.stream()
                    .filter(x -> x.getMembers().contains(playerUUID) ||
                            x.getOperators().contains(playerUUID)).findFirst();
            return optionalSFaction.orElse(null);
        }finally {
            lock.unlock();
        }
    }

    /**
     * Gets a faction by members or operator.
     * @param sPlayer The member or operator of the faction.
     * @return The faction, null if not found.
     */
    public SFaction getBySPlayer(SPlayer sPlayer){
        return getByPlayerUUID(sPlayer.getUuid());
    }

    public void addSFaction(SFaction sFaction) {
        if(getByUUID(sFaction.getUuid()) == null){
            try{
                lock.lock();
                factions.add(sFaction);
            } finally {
                lock.unlock();
            }
        } else {
            Bukkit.getLogger().warning("Something tried to add an already existing faction.");
        }
    }

    /**
     * Method used in order to remove a faction from the registered factions.
     * @param sFaction The faction to remove.
     */
    public void removeSFaction(SFaction sFaction){
        try{
            lock.lock();
            factions.remove(sFaction);
        } finally {
            lock.unlock();
        }
    }
}
